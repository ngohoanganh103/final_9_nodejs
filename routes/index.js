const flightRoute = require('./flights')
const siteRoute = require('./sites')
const airlineController = require('../controllers/AirlineController');
const airportController = require('../controllers/AirportController');
const adminRoute = require('./admin')

function initRoute(app) {
    // [GET,POST,PUT,....] Get flights
    app.use('/select-flight',  flightRoute);
    // [GET] home page
    app.use('/',siteRoute); 
    //Admin page
    app.use('/admin', adminRoute)
}
module.exports =  initRoute ;